/*
    Arrays and indides (plural for index)

    Arrays are used to store multiple related values in a single variable.

    Arrays are declared using square brackets [], also known as Array Literals.

    Array['element0','element1', 'element2', 'element3']

    index refer to element position inside an array

    Array[0] = 'element0'
    Array[1] = 'element1'
    Array[2] = 'element2'
    Array[3] = 'element3'
 */

let grades = [98.5, 94.3, 89.2, 90.1];
// Adding new element in an array
grades[grades.length] = 99.8;

let computerBrands = [
  "Acer",
  "Asus",
  "Lenovo",
  "Neo",
  "Redfox",
  "Gateway",
  "Toshiba",
  "Fujitsu",
];

// console.log(computerBrands[3]);

let singleElement = [1];

let emptyArr = [];

let example1 = ["cat", "cat", "cat"];

/*
    READING FROM / ACCESSING ELEMENTS INSIDE OF AN ARRAY

    To access an array element, simply denote the elements index number indside

    Getting and index of and element in an Array

    if(emails.indexOf("myEmail@email.com") === -1){
        register here, email not found
    }

console.log(grades.indexOf(89.2)); // returns 2
console.log(grades.indexOf(70)); // returns -1, cause can't find it inside current value of array grades    

    */

/*
    Array methods
*/

// .push() - Adding new element in an array

console.log(grades.push(88.8));
console.log((grades[grades.length] = 99.8));
console.log(grades);

// .pop() - removes an aray's LAST element and returns the removed element
grades.pop();
console.log(grades);

// .unshift() - it will add an element to the beginning of an array and returns new array lenth
grades.unshift(77.7);
console.log(grades);

// .shift() - it will remove the FIRST element of an array and returns new array lenth
grades.shift();
console.log(grades);

let tasks = [
  "shower",
  "eat breakfast",
  "go to work",
  "go to home",
  "go to sleep",
];
// .splice() - can add and remove from anywhere in an array, and even do both at once
// syntax: arr.splice(starting index number, number of items to remove, items to add)
console.log(tasks);
tasks.splice(3, 0, "eat lunch"); // adding in specific position in an array
tasks.splice(3, 1, "code"); // adding and removing
tasks.splice(3, 1); // just removing
tasks.splice(3, 1, "study", "do coding"); // adding multiple element and remove 1 element
// console.log(tasks);

// .sort() - rearanges an array in an alphanumeric order
computerBrands.sort();
console.log(computerBrands);

// .reverse() - reverses an array in an alphanumeric order
computerBrands.reverse();
console.log(computerBrands);

let a = [1, 2, 3, 4, 5];
let b = [6, 7, 8, 9, 10];

let arr1 = [1, 2, 3, 4, 5];
let arr2 = [1, 2, 3, 4, 5];
console.log(arr1 === arr2); // false
console.log(arr1.join() === arr2.join()); // true

// .concat() - combines two arrays arr1.concat(arr2)
let ab = a.concat(b);
let joinArray = ab.join();
console.log(joinArray);
console.log(typeof joinArray);

// .slice() - copy range of elements in an array arr.slice(startIndex, endIndex)
// copy anything from startIndex upto BEFORE endIndex
let c = a.slice(0, 3);
console.log(c); // [1,2,3]

console.clear();
let animal = "hippopotamus";
animal.slice(0, 1);
console.log(animal.split("")); // transfomr string to array

let str = "supercalifragilisticexpialidocious";
let newStr = "";
// skip vowel, if not print it
for (let i = 0; i < str.length; i++) {
  if (
    str[i] === "a" ||
    str[i] === "e" ||
    str[i] === "i" ||
    str[i] === "o" ||
    str[i] === "u"
  ) {
    continue;
  }
  newStr += str[i];
}

console.log(newStr);

// copying even number from numbersArr to placeholderArr
let numbersArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let placeholderArr = numbersArr.filter((element) => element % 2 === 0);
console.log(placeholderArr);

// console.clear();
// ARRAY ITERATION METHODS
// JAVASCRIPT
// really love the simplicity of ES6
// arr.forEach(element, index)

computerBrands.forEach((brand) => console.log(brand));

// creates new array
console.log(
  "Map: ",
  numbersArr.map((element) => element % 2 === 0)
);

// .every() Returns true if every element in this array satisfies the testing function.
console.log(
  "Every: ",
  numbersArr.every((element) => element % 2 === 1)
); // false cause we have odd number in numberArr

// .some() Returns true if at least one element in this array satisfies the provided testing function.
console.log(
  "Some: ",
  numbersArr.some((element) => element === 10)
);

let words = ["spray", "limit", "elite", "exuberant", "destruction", "present"];
let wordResult = words.filter((word) => word.length > 6);
console.log(wordResult);

// .includes() check if an array contains an element
console.log("includes()", words.includes("Spray")); // false
console.log("includes()", words.includes("spray")); // true

let twoDimensional = [
  [1.1, 1.2, 1.3],
  [2.1, 2.2, 2.3],
  [3.1, 3.2, 3.3],
];
console.log(twoDimensional[0][1]);
