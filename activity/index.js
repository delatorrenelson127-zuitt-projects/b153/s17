// Activity
let students = [];

// Add Student
function addStudent(str) {
  str = str.split("").join(""); // removing spaces from input

  let isFound = students
    .map((element) => element[0].toLowerCase()) // convert to lowercase
    .includes(str.toLowerCase()); // check if already exist

  if (!isFound) {
    // if not found in the array then push it
    students.push([str]);
    console.log(`${str} is added to the student's list.`);
  } else {
    console.log(`${str} is already exist.`);
  }
}

// Count students
function countStudents() {
  console.log(`There are total of ${students.length} students enrolled.`);
}

// Print students
function printStudents() {
  console.log("--- STUDENTS LIST ---");
  students.forEach((element) => console.log(element.join(" - ")));
  console.log("--- ************* ---");
}

// Find Student
function findStudent(str) {
  str = str.split("").join(""); // removing spaces from input

  console.log("FOUND: ");
  students
    .filter((element) => element[0].toLowerCase().includes(str.toLowerCase()))
    .map((element) => element.join(" - "))
    .forEach((element) => {
      console.log(element);
    });
}

// Add Section
function addSection(str) {
  str = str.split("").join(""); // removing spaces from input

  students.forEach(function (element, i) {
    if (!element.includes(` - `)) {
      students[i][1] = str.toUpperCase();
    }
  });
}

// Remove Student
function removeStudent(str) {
  str = str.split("").join(""); // removing spaces from input

  students = students.filter(
    (element) => element[0].toLowerCase() !== str.toLowerCase()
  );
  console.log(`${str} was removed fromt the student's list.`);
}
